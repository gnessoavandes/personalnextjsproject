
import java.util.Random;
import java.util.*;

class Machine {
	 int totalPistons = 100;
     int pistonsAssembled = 0;
     int headsAssembled = 0;
     int skirtsAssembled = 0;
     int axesAssembled = 0;
     
    private final String name;
    private final double breakdownProbability;
    private final int repairTime;

    public Machine(String name, double breakdownProbability, int repairTime) {
        this.name = name;
        this.breakdownProbability = breakdownProbability;
        this.repairTime = repairTime;
    }

    public boolean isBroken() {
        Random random = new Random();
        return random.nextDouble() <= breakdownProbability;
    }

    public int getRepairTime() {
        Random random = new Random();
        return random.nextInt(repairTime) + 5;
    }

    public String getName() {
        return name;
    }

    public int process() {
        if (isBroken()) {
            int repairTime = getRepairTime();
//            System.out.println(" broke down! Repair time for MA: " + repairTime + " minutes");
//            System.out.println(" broke down! Repair time for MT: " + repairTime + " minutes");
            System.out.println(getName() + " broke down! Repair time: " + repairTime + " minutes");
            return repairTime;
        } else {
            return 1; // Processing time is 1 minute
        }
    }
}

class Piston {
    private Machine machineTete;
    private Machine machineJupe;
    private Machine machineAxe;
    private Machine machineMP;

    public Piston(Machine machineTete, Machine machineJupe, Machine machineAxe, Machine machineMP) {
        this.machineTete = machineTete;
        this.machineJupe = machineJupe;
        this.machineAxe = machineAxe;
        this.machineMP = machineMP;
    }

    public int assemble() {
        int totalTime = 0;
        totalTime += machineTete.process();
        totalTime += machineJupe.process();
        totalTime += machineAxe.process();
        totalTime += machineMP.process();
        return totalTime;
    }
    
    
}

public class PistonAssemblySimulation {
    public static void main(String[] args) {
        Machine machineTete = new Machine("Machine MT", 0.25, 6);
        Machine machineJupe = new Machine("Machine MJ", 0.25, 6);
        Machine machineAxe = new Machine("Machine MA", 0.25, 6);
        Machine machineMP = new Machine("Machine MP", 0.25, 6);

        int pistonsAssembled = 0;
        int totalAssemblyTime = 0;
        //give input
        Scanner sc=new Scanner(System.in);
       System.out.println("1.Temps de reparation MA");
       System.out.println("2.Temps de reparation MT");
       System.out.println("3.Temps de reparation MP");
       System.out.println("4.Temps de reparation MJ");
       

        while (pistonsAssembled < 100) {
            Piston piston = new Piston(machineTete, machineJupe, machineAxe, machineMP);
            totalAssemblyTime += piston.assemble();
            pistonsAssembled++;
        }

        System.out.println("Total assembly time for 100 pistons: " + totalAssemblyTime + " minutes");
    }
}
